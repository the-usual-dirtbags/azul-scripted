--[[ Lua code. See documentation: https://api.tabletopsimulator.com/ --]]
tileBagGUID = 'd101ec'
discardBagGUID = '68e703'
dealButtonGUID = "8cea79"
hasGameStarted = false
factoryDisplayGUIDs = {}

--[[ The onLoad event is called after the game save finishes loading. --]]
function onLoad(saved_data)

-- In order for the save and rewind buttons to function we
-- need to make sure our local variables are saved properly
  if(saved_data ~= "") then
    local loaded_data = JSON.decode(saved_data)
    hasGameStarted = loaded_data.started
    if hasGameStarted then
        factoryDisplayGUIDs = loaded_data.factoryDisplayGUIDs
    end
  end

-- Sets the snap points for the scoring area on each board
-- TODO: migrate the snap points that are set on individual boards here
  setBoardSnaps()

-- start gaem button
  if not(hasGameStarted) then
      startPawn = getObjectFromGUID('45cea7')

      startPawn.createButton({
          click_function = 'startGame',
          label = "Start Game",
          function_owner = self,
          position       = {0, 0 ,5},
          rotation       = {0, 90, 0},
          width          = 3000,
          height         = 800,
          font_size      = 400,
          color          = {1,1, 1},
          font_color     = {0, 0, 0},
          tooltip        = "Wait until all players are seated to start."
        })
    else
      createDealButton()
    end
end

--[[ The onUpdate event is called once per frame. --]]
function onUpdate()
    --[[ print('onUpdate loop!') --]]
end


function onSave()
  --Save data for future loading
  if hasGameStarted then
      local data_to_save = {
        started = hasGameStarted,
        Players = getSeatedPlayers(),
        factoryDisplayGUIDs = factoryDisplayGUIDs
        }
    saved_data = JSON.encode(data_to_save)
    return saved_data
  end
end

-- This deals out the displays and then triggers each display to get tiles
-- TODO: Should this be a button for the user to select the number of players,
-- instead of using the number of seated players?
function dealDisplays(playerCount)
  Players = getSeatedPlayers()
  playerCount = playerCount or #Players

  displayCounts = {
    [1] = 9, [2] = 5, [3] = 7, [4] = 9
  }


  factoryDisplayStack = getObjectFromGUID('914fda')


  for i = 1, displayCounts[playerCount] do
    basex = -10
    basez = 10
    x = basex + 10*(i%3)
    y = 5
    z = basez - 10*(math.ceil(i/3))
    factoryDisplayStack.takeObject({
      position = {x, y, z},
      callback_function = function(obj) dealtDisplay(obj) end,
    })
  end
end

-- callback function that populates each display with tiles
function dealtDisplay(display)
  table.insert(factoryDisplayGUIDs, display.getGUID())
  display.setLock(true)
  dealTiles(display)
end

function startGame(buttonObj)
  hasGameStarted = true
  dealDisplays()

  createDealButton()
  buttonObj.removeButton(0)
end

function createDealButton()
  -- Add a button to deal the new round
  tileBag = getObjectFromGUID(tileBagGUID)
  tileBag.shuffle()

  tileBag.createButton({
      click_function = 'newRound',
      label = "Deal Tiles",
      function_owner = self,
      position       = {-6, 0.5,5},
      rotation       = {0, 0, 0},
      width          = 2800,
      height         = 800,
      font_size      = 340,
      color          = {1,1, 1},
      font_color     = {0, 0, 0},
      tooltip        = "Deals tiles to the factory displays and shuffles in discard bag if needed."
    })

end

function newRound()

  tileBag = getObjectFromGUID(tileBagGUID)
  tileBag.shuffle()

  for __, displayGUID in ipairs(factoryDisplayGUIDs) do
    -- There's a race condition on reshuffling the tiles that we need to delay for.
    -- TODO: find a better way to do this
    if tileBag.getQuantity() < 4 then
      recycleTiles()
      Wait.time(function() fillDisplay(displayGUID) end, 2)
    else
      fillDisplay(displayGUID)
    end

  end
end

function fillDisplay(displayGUID)

     display = getObjectFromGUID(displayGUID)
     dealTiles(display)
end

function dealTiles(display)
  tileBag = getObjectFromGUID(tileBagGUID)
  displayPos = display.getPosition()
  targetPositions = {
     {2.3, 0.0, 0},
     {-2.3, 0.0, 0},
     {0, 0.0, -2.3},
     {0, 0.0, 2.3},
  }


  for i = 1, 4 do
    tilePosition = display.positionToWorld(targetPositions[i])
    tileBag.takeObject({position = tilePosition, rotation={0,0,0}})
  end
end

-- Move tiles from the discard bag to the draw bag
function recycleTiles()
  tileBag = getObjectFromGUID(tileBagGUID)
  discardBag = getObjectFromGUID(discardBagGUID)
  discardBagContent = discardBag.getObjects()
  log(discardBagContent)

  for __, v in ipairs(discardBagContent) do
    tileBag.putObject(discardBag.takeObject({ guid = v.guid }))
  end
end

function setBoardSnaps()
  scoresnaps = {}
  for row = 1, 5 do
      for i=1, 20 do
          currentx = 7.8 - 0.82*(i-1)

          currenty = -7.7 + 1*(row-1)
          snapdex = (row-1)*20+i
          scoresnaps[snapdex] = { position = {currentx, 0.0, currenty}, rotation = {0,0,0}, rotation_snap = true }
      end
  end

  boards = {getObjectFromGUID('18a461'),getObjectFromGUID('9f42ee'),getObjectFromGUID('8259b9'),getObjectFromGUID('4b40cd')}
  for __, board in ipairs(boards) do
    board.setSnapPoints(scoresnaps)
  end

end