# TTS - Azul
TTS - Azul is a LUA script written for [Tabletop Simulator](https://www.tabletopsimulator.com). Please note that this repository only contains the source code for the project and does not include the corresponding assets.

## Getting Started
Once you have everything set up, you can get started working on the project via the following guidelines:
1. Subscribe to the [Azul workshop mod](https://steamcommunity.com/sharedfiles/filedetails/?id=2030586158).
2. Start Tabletop Simulator.
3. Open up Atom (with the TTS plugin already installed).
4. Create a new server.
5. Load the workshop mod (Games > Workshop > Codenames).
6. Open up Atom. The script should already be loaded in the editor. If not, use the `CTRL` + `SHIFT` + `L` key combination to get LUA scripts from the game.
7. Clone the repository and copy the contents of the files from the repository into their respectively named files on your local instance (usually something like `Temp/Tabletop Simulator/Tabletop Simulator Lua Scripts`.
8. Use `CTRL` + `SHIFT` + `S` to load the files into the game
9. Make / test / save your changes
10. Copy your changes into the directory containing the repository
11. Check in your changes
